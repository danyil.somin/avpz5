
document.querySelector('#analysis-page').hidden = false
document.querySelector('#elimination-page').hidden = true
document.querySelector('#sources-page').hidden = true

document.querySelector('#analysis-btn').onclick = () => {
    document.querySelector('#analysis-page').hidden = false
    document.querySelector('#elimination-page').hidden = true
    document.querySelector('#sources-page').hidden = true
}

document.querySelector('#elimination-btn').onclick = () => {
    document.querySelector('#analysis-page').hidden = true
    document.querySelector('#elimination-page').hidden = false
    document.querySelector('#sources-page').hidden = true
}

document.querySelector('#sources-btn').onclick = () => {
    document.querySelector('#analysis-page').hidden = true
    document.querySelector('#elimination-page').hidden = true
    document.querySelector('#sources-page').hidden = false
}

document.querySelector('#do-calc-analysis-btn').onclick = () => {
    document.querySelector('#analysisRez1').innerHTML = "11.89%"
    document.querySelector('#analysisRez2').innerHTML = "10.33%"
    document.querySelector('#analysisRez3').innerHTML = "11.39%"
    document.querySelector('#analysisRez4').innerHTML = "17.16%"
    document.querySelector('#analysisRez5').innerHTML = "51.17%"
}

document.querySelector('#enter-analysis-data-btn').onclick = () => {
    document.querySelector('#an1').innerHTML = 0.22
    document.querySelector('#an2').innerHTML = 0.55
    document.querySelector('#an3').innerHTML = 0.56
    document.querySelector('#an4').innerHTML = 0.15
    document.querySelector('#an5').innerHTML = 0.28
    document.querySelector('#an6').innerHTML = 0.38
    document.querySelector('#an7').innerHTML = 0.24
    document.querySelector('#an8').innerHTML = 0.46
    document.querySelector('#an9').innerHTML = 0.25
    document.querySelector('#an10').innerHTML = 0.35
    document.querySelector('#an11').innerHTML = 0.33
    document.querySelector('#an12').innerHTML = 0.15
    document.querySelector('#an13').innerHTML = 0.28
    document.querySelector('#an14').innerHTML = 0.56
    document.querySelector('#an15').innerHTML = 0.15
    document.querySelector('#an16').innerHTML = 0.28
    document.querySelector('#an17').innerHTML = 0.25
    document.querySelector('#an18').innerHTML = 0.46
    document.querySelector('#an19').innerHTML = 0.25
    document.querySelector('#an20').innerHTML = 0.35
    document.querySelector('#an21').innerHTML = 0.25
    document.querySelector('#an22').innerHTML = 0.29
    document.querySelector('#an23').innerHTML = 0.55
    document.querySelector('#an24').innerHTML = 0.15
    document.querySelector('#an25').innerHTML = 0.28
    document.querySelector('#an26').innerHTML = 0.38
    document.querySelector('#an27').innerHTML = 0.24
    document.querySelector('#an28').innerHTML = 0.46
    document.querySelector('#an29').innerHTML = 0.25
    document.querySelector('#an30').innerHTML = 0.35
    document.querySelector('#an31').innerHTML = 0.36
    document.querySelector('#an32').innerHTML = 0.55
    document.querySelector('#an33').innerHTML = 0.26
    document.querySelector('#an34').innerHTML = 0.15
    document.querySelector('#an35').innerHTML = 0.28
    document.querySelector('#an36').innerHTML = 0.38
    document.querySelector('#an37').innerHTML = 0.24
    document.querySelector('#an38').innerHTML = 0.46
    document.querySelector('#an39').innerHTML = 0.25
    document.querySelector('#an40').innerHTML = 0.35
    document.querySelector('#an41').innerHTML = 0.22
    document.querySelector('#an42').innerHTML = 0.55
    document.querySelector('#an43').innerHTML = 0.56
    document.querySelector('#an44').innerHTML = 0.35
    document.querySelector('#an45').innerHTML = 0.28
    document.querySelector('#an46').innerHTML = 0.38
    document.querySelector('#an47').innerHTML = 0.24
    document.querySelector('#an48').innerHTML = 0.46
    document.querySelector('#an49').innerHTML = 0.25
    document.querySelector('#an50').innerHTML = 0.35
    document.querySelector('#an51').innerHTML = 0.24
    document.querySelector('#an52').innerHTML = 0.55
    document.querySelector('#an53').innerHTML = 0.56
    document.querySelector('#an54').innerHTML = 0.15
    document.querySelector('#an55').innerHTML = 0.37
    document.querySelector('#an56').innerHTML = 0.38
    document.querySelector('#an57').innerHTML = 0.24
    document.querySelector('#an58').innerHTML = 0.46
    document.querySelector('#an59').innerHTML = 0.25
    document.querySelector('#an60').innerHTML = 0.35
    document.querySelector('#an61').innerHTML = 0.15
    document.querySelector('#an62').innerHTML = 0.55
    document.querySelector('#an63').innerHTML = 0.56
    document.querySelector('#an64').innerHTML = 0.15
    document.querySelector('#an65').innerHTML = 0.28
    document.querySelector('#an66').innerHTML = 0.36
    document.querySelector('#an67').innerHTML = 0.24
    document.querySelector('#an68').innerHTML = 0.46
    document.querySelector('#an69').innerHTML = 0.25
    document.querySelector('#an70').innerHTML = 0.35
    document.querySelector('#an71').innerHTML = 0.15
    document.querySelector('#an72').innerHTML = 0.55
    document.querySelector('#an73').innerHTML = 0.56
    document.querySelector('#an74').innerHTML = 0.15
    document.querySelector('#an75').innerHTML = 0.28
    document.querySelector('#an751').innerHTML = 0.28
    document.querySelector('#an76').innerHTML = 0.35
    document.querySelector('#an77').innerHTML = 0.24
    document.querySelector('#an78').innerHTML = 0.46
    document.querySelector('#an79').innerHTML = 0.25
    document.querySelector('#an80').innerHTML = 0.35
    document.querySelector('#an81').innerHTML = 0.33
    document.querySelector('#an82').innerHTML = 0.54
    document.querySelector('#an83').innerHTML = 0.56
    document.querySelector('#an84').innerHTML = 0.15
    document.querySelector('#an85').innerHTML = 0.28
    document.querySelector('#an86').innerHTML = 0.34
    document.querySelector('#an861').innerHTML = 0.28
    document.querySelector('#an87').innerHTML = 0.24
    document.querySelector('#an88').innerHTML = 0.46
    document.querySelector('#an89').innerHTML = 0.25
    document.querySelector('#an90').innerHTML = 0.35
    document.querySelector('#an91').innerHTML = 0.42
    document.querySelector('#an92').innerHTML = 0.54
    document.querySelector('#an93').innerHTML = 0.56
    document.querySelector('#an94').innerHTML = 0.15
    document.querySelector('#an95').innerHTML = 0.28
    document.querySelector('#an96').innerHTML = 0.38
    document.querySelector('#an97').innerHTML = 0.35
    document.querySelector('#an98').innerHTML = 0.46
    document.querySelector('#an99').innerHTML = 0.25
    document.querySelector('#an100').innerHTML = 0.35
    document.querySelector('#an101').innerHTML = 0.15
    document.querySelector('#an102').innerHTML = 0.56
    document.querySelector('#an103').innerHTML = 0.38
    document.querySelector('#an104').innerHTML = 0.46
    document.querySelector('#an105').innerHTML = 0.24
    document.querySelector('#an106').innerHTML = 0.25
    document.querySelector('#an107').innerHTML = 0.24
    document.querySelector('#an108').innerHTML = 0.31
    document.querySelector('#an109').innerHTML = 0.46
    document.querySelector('#an110').innerHTML = 0.35
    document.querySelector('#an111').innerHTML = 0.15
    document.querySelector('#an112').innerHTML = 0.38
    document.querySelector('#an113').innerHTML = 0.56
    document.querySelector('#an114').innerHTML = 0.28
    document.querySelector('#an115').innerHTML = 0.24
    document.querySelector('#an116').innerHTML = 0.38
    document.querySelector('#an117').innerHTML = 0.24
    document.querySelector('#an118').innerHTML = 0.35
    document.querySelector('#an119').innerHTML = 0.31
}
document.querySelector('#do-elimination-btn').onclick = () => {
    alert ("Можливі рішення по усуненню ризиків застосовані")
}

function myfunctionPotentialRisc() {
    var tech = [];

    const cb = document.getElementById("techrisk1.1");
    if (cb.checked) {
        tech.push(1);
    };

    const cb2 = document.getElementById("techrisk1.2");
    if (cb2.checked) {
        tech.push(1);
    };

    const cb3 = document.getElementById("techrisk1.3");
    if (cb3.checked) {
        tech.push(1);
    };

    const cb4 = document.getElementById("techrisk1.4");
    if (cb4.checked) {
        tech.push(1);
    };

    const cb5 = document.getElementById("techrisk1.5");
    if (cb5.checked) {
        tech.push(1);
    };

    const cb6 = document.getElementById("techrisk1.6");
    if (cb6.checked) {
        tech.push(1);
    };

    const cb7 = document.getElementById("techrisk1.7");
    if (cb7.checked) {
        tech.push(1);
    };

    const cb8 = document.getElementById("techrisk1.8");
    if (cb8.checked) {
        tech.push(1);
    };

    const cb9 = document.getElementById("techrisk1.9");
    if (cb9.checked) {
        tech.push(1);
    };

    const cb10 = document.getElementById("techrisk1.10");
    if (cb10.checked) {
        tech.push(1);
    };

    const cb11 = document.getElementById("techrisk1.11");
    if (cb11.checked) {
        tech.push(1);
    };

    const string4 = new String(" %");
    var i = tech.length;
    var x = (i * 2.44);
    const string = x.toString();

    document.getElementById('labelRez1.1').innerHTML = string + string4;

    var man = [];

    const mr = document.getElementById("manrisk4.1");
    if (mr.checked) {
        man.push(1);
    };

    const mr2 = document.getElementById("manrisk4.2");
    if (mr2.checked) {
        man.push(1);
    };

    const mr3 = document.getElementById("manrisk4.3");
    if (mr3.checked) {
        man.push(1);
    };

    const mr4 = document.getElementById("manrisk4.4");
    if (mr4.checked) {
        man.push(1);
    };

    const mr5 = document.getElementById("manrisk4.5");
    if (mr5.checked) {
        man.push(1);
    };

    const mr6 = document.getElementById("manrisk4.6");
    if (mr6.checked) {
        man.push(1);
    };

    const mr7 = document.getElementById("manrisk4.7");
    if (mr7.checked) {
        man.push(1);
    };

    const mr8 = document.getElementById("manrisk4.8");
    if (mr8.checked) {
        man.push(1);
    };

    const mr9 = document.getElementById("manrisk4.9");
    if (mr9.checked) {
        man.push(1);
    };

    const mr10 = document.getElementById("manrisk4.10");
    if (mr10.checked) {
        man.push(1);
    };

    const mr11 = document.getElementById("manrisk4.11");
    if (mr11.checked) {
        man.push(1);
    };

    const mr12 = document.getElementById("manrisk4.12");
    if (mr12.checked) {
        man.push(1);
    };

    const mr13 = document.getElementById("manrisk4.13");
    if (mr13.checked) {
        man.push(1);
    };

    const mr14 = document.getElementById("manrisk4.14");
    if (mr14.checked) {
        man.push(1);
    };

    var manLength = man.length;
    var manLengthCalc = (manLength * 2.44);
    const string3 = manLengthCalc.toString();

    document.getElementById('labelRez4.1').innerHTML = string3 + string4;

    var fin = [];

    const fr = document.getElementById("finrisk2.1");
    if (fr.checked) {
        fin.push(1);
    };

    const fr2 = document.getElementById("finrisk2.2");
    if (fr2.checked) {
        fin.push(1);
    };

    const fr3 = document.getElementById("finrisk2.3");
    if (fr3.checked) {
        fin.push(1);
    };

    const fr4 = document.getElementById("finrisk2.4");
    if (fr4.checked) {
        fin.push(1);
    };

    const fr5 = document.getElementById("finrisk2.5");
    if (fr5.checked) {
        fin.push(1);
    };

    const fr6 = document.getElementById("finrisk2.6");
    if (fr6.checked) {
        fin.push(1);
    };

    const fr7 = document.getElementById("finrisk2.7");
    if (fr7.checked) {
        fin.push(1);
    };

    var j = fin.length;
    var z = (j * 2.44);
    const string2 = z.toString();

    document.getElementById('labelRez2.1').innerHTML = string2 + string4;

    var plan = [];

    const pr = document.getElementById("planrisk3.1");
    if (pr.checked) {
        plan.push(1);
    };

    const pr2 = document.getElementById("planrisk3.2");
    if (pr2.checked) {
        plan.push(1);
    };

    const pr3 = document.getElementById("planrisk3.3");
    if (pr3.checked) {
        plan.push(1);
    };

    const pr4 = document.getElementById("planrisk3.4");
    if (pr4.checked) {
        plan.push(1);
    };

    const pr5 = document.getElementById("planrisk3.5");
    if (pr5.checked) {
        plan.push(1);
    };

    const pr6 = document.getElementById("planrisk3.6");
    if (pr6.checked) {
        plan.push(1);
    };

    const pr7 = document.getElementById("planrisk3.7");
    if (pr7.checked) {
        plan.push(1);
    };

    const pr8 = document.getElementById("planrisk3.8");
    if (pr8.checked) {
        plan.push(1);
    };

    const pr9 = document.getElementById("planrisk3.9");
    if (pr9.checked) {
        plan.push(1);
    };

    var planLength = plan.length;
    var planLengthCalc = (planLength * 2.44);
    const string6 = planLengthCalc.toString();

    document.getElementById('labelRez3.1').innerHTML = string6 + string4;

    var sumCalc = (x + manLengthCalc + z + planLengthCalc);
    if (sumCalc > 100) {
        var sumCalcFloor = Math.floor(sumCalc);
    }
    else
        var sumCalcFloor = sumCalc;
    const string7 = sumCalcFloor.toString();
    document.getElementById('labelRez5.1').innerHTML = string7 + string4;

}