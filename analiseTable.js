const marksCount=10;
const riscs = [
    [
        "Технічні ризики",
        "Затримки у постачанні обладнання, необхідного для підтримки процесу розроблення ПЗ",
        "Затримки у постачанні інструментальних засобів, необхідних для підтримки процесу розроблення ПЗ",
        "Небажання команди виконавців ПЗ використовувати інструментальні засоби для підтримки процесу розроблення ПЗ",
        "Відмова команди виконавців від CASE-засобів розроблення ПЗ",
        "Формування запитів на більш потужні інструментальні засоби розроблення ПЗ",
        "Недостатня продуктивність баз(и) даних для підтримки процесу розроблення ПЗ",
        "Програмні компоненти, які використовують повторно в ПЗ, мають дефекти та обмежені функціональні можливості",
        "Неефективність програмного коду, згенерованого CASE-засобами розроблення ПЗ",
        "Неможливість інтеграції CASE-засобів з іншими інструментальними засобами для підтримки процесу розроблення ПЗ",
        "Швидкість виявлення дефектів у програмному коді є нижчою від раніше запланованих термінів",
        "Поява дефектних системних компонент, які використовують для розроблення ПЗ"
    ], [
        "Вартісні ризики",
        "Недо(пере)оцінювання витрат на реалізацію програмного проекту (надмірно низька вартість)",
        "Фінансові ускладнення у компанії-замовника ПЗ",
        "Фінансові ускладнення у компанії-розробника ПЗ",
        "Змен(збіль)шення бюджету програмного проекта з ініціативи компанії-замовника ПЗ під час його реалізації",
        "Висока вартість виконання повторних робіт, необхідних для зміни вимог до ПЗ",
        "Реорганізація структурних підрозділів у компанії-замовника ПЗ",
        "Реорганізація команди виконавців у компанії-розробника ПЗ",
    ], [
        "Планові ризики",
        "Зміни графіка виконання робіт з боку замовника чи виконавця",
        "Порушення графіка виконання робіт у компанії-розробника ПЗ",
        "Потреба зміни користувацьких вимог до ПЗ з боку компанії-замовника ПЗ",
        "Потреба зміни функціональних вимог до ПЗ з боку компанії-розробника ПЗ",
        "Потреба виконання великої кількості повторних робіт, необхідних для зміни вимог до ПЗ",
        "Недо(пере)оцінювання тривалості етапів реалізації програмного проекту з боку компанії-замовника ПЗ",
        "Остаточний розмір ПЗ значно перевищує (менший від) заплановані(их) його характеристики",
        "Поява на ринку аналогічного ПЗ до виходу замовленого",
        "Поява на ринку більш конкурентоздатного ПЗ"
    ], [
        "Ризики управління",
        "Низький моральний стан персоналу команди виконавців ПЗ",
        "Низька взаємодія між членами команди виконавців ПЗ",
        "Пасивність керівника (менеджера) програмного проекту",
        "Недостатня компетентність керівника (менеджера) програмного проекту",
        "Незадоволеність замовника результатами етапів реалізації програмного проекту",
        "Недостатня кількість фахівців у команді виконавців ПЗ з необхідним професійним рівнем",
        "Хвороба провідного виконавця в найкритичніший момент розроблення ПЗ",
        "Одночасна хвороба декількох виконавців підчас розроблення ПЗ",
        "Неможливість організації необхідного навчання персоналу команди виконавців ПЗ",
        "Зміна пріоритетів у процесі управління програмним проектом",
        "Недо(пере)оцінювання необхідної кількості розробників (підрядників і субпідрядників) на етапах життєвого циклу розроблення ПЗ",
        "Недостатнє (надмірне) документування результатів на етапах реалізації програмного проекту",
        "Нереалістичне прогнозування результатів на етапах реалізації програмного проекту",
        "Недостатній професійний рівень представників від компанії-замовника ПЗ"
    ]
];
var rowBaseIndex="rowAnalise";
var ERBaseIndex="rowResult";
var RezBaseIndex="labelProbRez"
var LosesBaseIndex="rowResultLoses"
var losesRowBaseIndex="rowLosses";
function AddRows() {
    var table=document.getElementById("AnaliseTable");
    table.innerHTML=" ";
    var er=0;
    var erIndex=0;
    riscs.forEach(function (riscPart,index,riscs) {
        riscPart.forEach(function (risc,i,riscPart) {
            var row=document.createElement("tr")
            var cell=document.createElement("td");
            if (i>0){
                cell.appendChild(document.createTextNode(i.toString()))
            }
            row.appendChild(cell);
            cell=document.createElement("td");
            cell.appendChild(document.createTextNode( riscPart[i]));
            row.appendChild( cell);
            if (i>0){
                for (let j=0;j<marksCount;++j) {
                    cell = document.createElement("td");
                    let mark = Math.random();
                    er += mark;
                    //cell.appendChild(document.createTextNode(mark.toFixed(2)));
                    var cellText=document.createElement("input")
                    cellText.setAttribute('type',"text");
                    cellText.setAttribute('class',"border-0 m-0 input-sm form-control")
                    cellText.setAttribute('style',"width:100%;height:100%;");
                    cellText.setAttribute('value',mark.toFixed(2).toString());
                    cellText.setAttribute('onchange',String("CalculateEr("+(erIndex).toString()+")"))
                    cell.appendChild(cellText);
                    cell.setAttribute('style',"vertical-align: middle;");
                    cell.setAttribute('class',"p-0");
                    row.appendChild(cell);
                }
                if(marksCount>0) {
                    er /= marksCount
                    cell=document.createElement("td");
                    cell.appendChild(document.createTextNode(er.toFixed(2)));
                    cell.setAttribute('id',String(ERBaseIndex+(erIndex).toString()));
                    cell.setAttribute('style',"vertical-align:middle");
                    row.appendChild(cell);
                }else {
                    alert("Error!.Marks count=0");
                }

                var rowId=rowBaseIndex+erIndex.toString();
                ++erIndex;
                row.setAttribute('id', rowId.toString());
            }
            table.appendChild(row)
        })
    })
}


function AddRowsLosses() {
    var table=document.getElementById("lossesTable");
    table.innerHTML=" ";
    var er=0;
    var erIndex=0;
    riscs.forEach(function (riscPart,index,riscs) {
        riscPart.forEach(function (risc,i,riscPart) {
            var row=document.createElement("tr")
            var cell=document.createElement("td");
            if (i>0){
                cell.appendChild(document.createTextNode(i.toString()))
            }
            row.appendChild(cell);
            cell=document.createElement("td");
            cell.appendChild(document.createTextNode( riscPart[i]));
            row.appendChild( cell);
            if (i>0){
                for (let j=0;j<marksCount;++j) {
                    cell = document.createElement("td");
                    let mark = Math.random();
                    er += mark;
                    //cell.appendChild(document.createTextNode(mark.toFixed(2)));
                    var cellText=document.createElement("input")
                    cellText.setAttribute('type',"text");
                    cellText.setAttribute('class',"border-0 m-0 input-sm form-control")
                    cellText.setAttribute('style',"width:100%;height:100%;");
                    cellText.setAttribute('value',mark.toFixed(2).toString());
                    cellText.setAttribute('onchange',String("CalculateLosses("+(erIndex).toString()+")"))
                    cell.appendChild(cellText);
                    cell.setAttribute('style',"vertical-align: middle;");
                    cell.setAttribute('class',"p-0");
                    row.appendChild(cell);
                }
                if(marksCount>0) {
                    er /= marksCount
                    cell=document.createElement("td");
                    cell.appendChild(document.createTextNode(er.toFixed(2)));
                    cell.setAttribute('id',String(LosesBaseIndex+(erIndex).toString()));
                    cell.setAttribute('style',"vertical-align:middle");
                    row.appendChild(cell);
                }else {
                    alert("Error!.Marks count=0");
                }
                var rowId=losesRowBaseIndex+(erIndex).toString();
                ++erIndex;
                row.setAttribute('id', rowId.toString());
            }
            table.appendChild(row)
        })
    })
}
function CalculateAvaragesLoses() {
   var RezLosesBaseIndex="labelRezLosses";
    var count=0;
    var fullRez=0.0;
    var erIndex=0;
    riscs.forEach(function (item,i,riscs){
        count+=item.length-1;
    })
    riscs.forEach(function (rows,index,riscs) {
        var sum=0.0;
        rows.forEach(function (row,j,rows){
            if(j!=0){
                var er=document.getElementById(String(LosesBaseIndex+(erIndex).toString()))
                ++erIndex;
                sum+=Number(er.innerText);
            }
        })
        sum/=count;
        var rez=document.getElementById(String(RezLosesBaseIndex+(index+1).toString()));
        rez.innerHTML=String((sum*100).toFixed(2)+"%");
        fullRez+=sum;
    })
    var rez=document.getElementById("labelFullRezLosses");
    rez.innerHTML=String((fullRez*100).toFixed(2)+"%");
}

function CalculateLosses( index){
    var RowCells=document.getElementById(String(losesRowBaseIndex+index)).cells;
    var sum=0.0
    for (var i=2;i<(RowCells.length-1);++i){
        sum+=Number(RowCells[i].firstChild.value);
    }
    var rezCell=document.getElementById(String(LosesBaseIndex+((index)).toString()));
    rezCell.innerHTML=(sum/marksCount).toFixed(2);
    //alert((sum/marksCount).toString());
    //alert(document.getElementById("analise").rows[2].cells[3].firstChild.value);
}
function CalculateEr( index){
    var RowCells=document.getElementById(String(rowBaseIndex+index)).cells;
    var sum=0.0
    for (var i=2;i<(RowCells.length-1);++i){
        sum+=Number(RowCells[i].firstChild.value);
    }
    var rezCell=document.getElementById(String(ERBaseIndex+((index)).toString()));
    rezCell.innerHTML=(sum/marksCount).toFixed(2);
    //alert((sum/marksCount).toString());
    //alert(document.getElementById("analise").rows[2].cells[3].firstChild.value);
}
function CalculateAvarages() {
    var count=0;
    var fullRez=0.0;
    var erIndex=0;
    riscs.forEach(function (item,i,riscs){
        count+=item.length-1;
    })
    riscs.forEach(function (rows,index,riscs) {
        var sum=0.0;
        rows.forEach(function (row,j,rows){
            if(j!=0){
                var er=document.getElementById(String(ERBaseIndex+(erIndex).toString()));
                ++erIndex
                sum+=Number(er.innerText);
            }
        })
        sum/=count;
        var rez=document.getElementById(String(RezBaseIndex+(index+1).toString()));
        rez.innerHTML=String((sum*100).toFixed(2)+"%");
        //rez.innerHTML=String(143234123);
        fullRez+=sum;
    })
    var rez=document.getElementById("labelFullRez");
    rez.innerHTML=String((fullRez*100).toFixed(2)+"%");

}
function AddMonitorRows(){
    var table=document.getElementById("MonitorTable");
    table.innerHTML=" ";
    var erIndex=0;
    riscs.forEach(function (riscPart,index,riscs) {
        riscPart.forEach(function (risc,i,riscPart) {
            var row = document.createElement("tr")
            var cell = document.createElement("td");
            if (i > 0) {
                cell.appendChild(document.createTextNode(i.toString()))
            }
            row.appendChild(cell);
            cell = document.createElement("td");
            cell.appendChild(document.createTextNode(riscPart[i]));
            row.appendChild(cell);
            if (i>0){
                cell=document.createElement("td");
                var probability=document.getElementById(String( ERBaseIndex+(erIndex).toString()));
                var losses=document.getElementById(String( LosesBaseIndex+(erIndex).toString()));
                ++erIndex;
                var riskValue=Number(probability.innerText)*Number(losses.innerText);
                cell.appendChild(document.createTextNode(probability.innerText));
                row.appendChild(cell);
                cell=document.createElement("td");
                cell.appendChild(document.createTextNode(losses.innerText));
                row.appendChild(cell);
                cell=document.createElement("td");
                cell.appendChild(document.createTextNode(riskValue.toFixed(2)));
                row.appendChild(cell);
                cell=document.createElement("td");
                if (riskValue<0.33){
                   cell.appendChild(document.createTextNode("Low"));
                }else if (riskValue<0.66){
                    cell.appendChild(document.createTextNode("Medium"));
                }else {
                    cell.appendChild(document.createTextNode("Hight"));
                }
                row.appendChild(cell);
                table.appendChild(row);

            }
        })
    })
}